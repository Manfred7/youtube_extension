var C_ANNOTATION = {
    allowType : {
        'highlight' : true
    },

    allowStyle : {
        'anchored' : true,
        'highlightText' : true,
        'label' : true,
        'title' : true,
        'popup' : true
    },

    idShablonMenuItem : 'fua-plagin-youtube-shablon-menu-item-id',

    idSavedAnnotationsBlock : 'fua-plagin-youtube-saved-annotations-block-id',

    idCloseSavedAnnotationsBlock :
        'fua-plagin-youtube-close-saved-annotations-block-id',

    idSaveAnnotationButton :
        'fua-plagin-youtube-save-annotation-button-id',

    idCopyMenuItem : 'fua-plagin-youtube-copy-menu-item-id',

    idCloseCopyAnnotationsBlock :
        'fua-plagin-youtube-close-copy-annotations-block-id'
};