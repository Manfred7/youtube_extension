// Image urls
var IMAGE_TIMER_URL = chrome.extension.getURL("img/timer.png");

var IMAGE_RIGHT_ARROW_URL =
    chrome.extension.getURL("img/right_arrow.png");

var IMAGE_DOWN_ARROW_URL =
    chrome.extension.getURL("img/down_arrow.png");

var IMAGE_RELOAD_URL =
    chrome.extension.getURL("img/reload.png");

var IMAGE_SHABLON_URL =
    chrome.extension.getURL("img/shablon.png");

var IMAGE_COPY_URL =
    chrome.extension.getURL("img/copy.png");

var IMAGE_SAVE_URL =
    chrome.extension.getURL("img/save.png");

var IMAGE_SAVE_GREEN_URL =
    chrome.extension.getURL("img/save_green.png");

var IMAGE_TRIANGLE_URL =
    chrome.extension.getURL("img/small_triangle.png");

var IMAGE_SCREENSHOT =
    chrome.runtime.getURL('img/navigation/screenshot.png');