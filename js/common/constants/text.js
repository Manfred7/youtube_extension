// yandex captcha
var C_TEXT_RELOAD_CAPTCHA =
    chrome.i18n.getMessage("reload_captcha");

var C_TEXT_CONTINUA_VERIFY_CAPTCHA  =
    chrome.i18n.getMessage("continua_verify_captcha") + '...';

var C_TEXT_CONTINUA_RELOADING_CAPTCHA  =
    chrome.i18n.getMessage("continua_reloading_captcha") + '....';

var C_TEXT_CAPTCHA_VERIFY_SUCCESS  =
    chrome.i18n.getMessage("captcha_verify_success");


// add our channel
var C_TEXT_ADD_OUR_CHANNEL_FIRST_HINT =
    chrome.i18n.getMessage("add_our_channel_first_hint");

var C_TEXT_ADD_OUR_CHANNEL_PANEL_TITLE =
    chrome.i18n.getMessage("add_our_channel_panel_title");



// annotations and cards
var C_SEARCH_ANNOTATION_PLACEHOLDER =
    chrome.i18n.getMessage("search_annotation_placeholder");

var C_TEXT_SAVE_ANNOTATION = chrome.i18n.getMessage("save_annotation");

var C_TEXT_COPY_ANNOTATION_FROM =
    chrome.i18n.getMessage("copy_annotation_from") + '...';

var C_TEXT_INSERT_COUNT_TIME =
    chrome.i18n.getMessage("insert_count_time") + ':';

var C_TEXT_FROM_START = chrome.i18n.getMessage("from_start");

var C_TEXT_FROM_END = chrome.i18n.getMessage("from_end");

var C_TEXT_COMPLETING_PATTERN =
    chrome.i18n.getMessage("completing_pattern");

var C_TEXT_TOOLTIP_SAVE_IN_PATTERN =
    chrome.i18n.getMessage("tooltip_save_in_pattern");

var C_TEXT_COPY_CARDS_FROM_TITLE =
    chrome.i18n.getMessage("copy_cards_from_title");

var C_TEXT_COPY_CARDS_FROM_DESCRIPTION =
    chrome.i18n.getMessage("copy_cards_from_description");

var C_TEXT_COMPLETED_CARDS_PATTERNS_TITLE=
    chrome.i18n.getMessage("completed_cards_patterns_title");

var C_TEXT_COMPLETED_CARDS_PATTERNS_DESCRIPTION=
    chrome.i18n.getMessage("completed_cards_patterns_description");




// watch video & watch channel
var C_TEXT_SHOW_TAGS = chrome.i18n.getMessage("show_tags");

var C_TEXT_HIDE_TAGS = chrome.i18n.getMessage("hide_tags");

var C_TEXT_THIS_VIDEO_TAGS_CATEGORY_TITLE =
    chrome.i18n.getMessage("this_video_tags_category_title");

var C_TEXT_THIS_CHANNEL_TAGS_CATEGORY_TITLE =
    chrome.i18n.getMessage("this_channel_tags_category_title");

var C_TEXT_COPY_TAGS_BUTTON = chrome.i18n.getMessage("copy_all_tags");

var C_TEXT_COPY_SELECTED_TAGS_BUTTON =
    chrome.i18n.getMessage("copy_selected_tags");

var C_TEXT_COPIED_TAGS_BUTTON = chrome.i18n.getMessage("tags_copied_yet");




// upload & edit video
var C_TEXT_ADD_TIME_CODE = chrome.i18n.getMessage("add_time_code");

var C_TEXT_SEARCH_TAGS_INPUT_PLACEHOLDER =
    chrome.i18n.getMessage("search_tags_input_placeholder");

var C_TEXT_GOOGLE_TRENDS = chrome.i18n.getMessage("google_trends");

var C_TEXT_YANDEX_KEYWORDS_TITLE  =
    chrome.i18n.getMessage("yandex_keywords_title");

var C_TEXT_TAGS_POPULARITY = chrome.i18n.getMessage("tags_popularity");

var C_TEXT_YENDEX_KEYWORD_VOLUME =
    chrome.i18n.getMessage("yendex_keyword_volume");




// common words & phrases
var C_TEXT_PLACE_IN_YOUTUBE_SEARCH =
    chrome.i18n.getMessage("place_in_youtube_search") + " - ";

var C_TEXT_RANKS_IN_YANDEX_KEYWORDS =
    chrome.i18n.getMessage("ranks_in_yandex_keywords") + " - ";

var C_TEXT_FOUND_NOTHING =
    chrome.i18n.getMessage("found_nothing") + '!!!';

var C_TEXT_WORD_SHOW = chrome.i18n.getMessage("word_show");

var C_TEXT_WORD_LEFT = chrome.i18n.getMessage("word_left") + ": ";

var C_TEXT_WORD_SYMBOLS = chrome.i18n.getMessage("word_symbols");

var C_TEXT_WORD_ADD = chrome.i18n.getMessage("word_add");

var C_TEXT_WORD_TAGS = chrome.i18n.getMessage("word_tags");

var C_TEXT_WORD_OR = chrome.i18n.getMessage("word_or");

var C_TEXT_WORD_DOUBLE_TAGS = chrome.i18n.getMessage("word_double_tags");

var C_TEXT_WORD_CLEAR = chrome.i18n.getMessage("word_clear");

var C_TEXT_WORD_SEARCH = chrome.i18n.getMessage("word_search");

var C_TEXT_WORD_HINTS = chrome.i18n.getMessage("word_hints");

var C_TEXT_WORD_YOUTUBE = chrome.i18n.getMessage("word_youtube");

var C_TEXT_WORD_ALL = chrome.i18n.getMessage("word_all");

var C_TEXT_WORD_YANDEX = chrome.i18n.getMessage("word_yandex");

var C_TEXT_WORD_VOLUME = chrome.i18n.getMessage("word_volume");

var C_TEXT_WORD_COPY = chrome.i18n.getMessage("word_copy");