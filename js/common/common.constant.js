// upload you tube panel
var ID_OUR_VIDEO_TIME_CODE_ICON =
    'fua-plugin-youtube_video_time_code_icon';



var ID_OUR_PANEL_AFTER_TITLE =
    "id_our_panel_after_title";

var ID_OUR_TITLE_LENGTH =
    "fua-plugin-youtube-title-length";



var ID_OUR_PANEL_AFTER_DESCRIPTION =
    "id_our_panel_after_description";

var ID_OUR_DESCRIPTION_LENGTH =
    "fua-plugin-youtube-description-length";



var ID_OUR_PANEL_AFTER_YOUTUBE_TAGS =
    "id_our_panel_after_youtube_tags";

var ID_OUR_TOTAL_TAGS_LENGTH =
    "fua-plugin-youtube-id_our_total_tags_length";

var ID_OUR_TAG_UPLOAD_CLEAR_BUTTON =
    "fua-plugin-youtube-upload-tags-clear-button";

var ID_OUR_TAG_SHOW_PLACE_IN_YOUTUBE_BOX =
    "fua-plugin-youtube-show_place_in_youtube_box";

var ID_OUR_TAG_SHOW_PLACE_IN_YOUTUBE =
    "fua-plugin-youtube-show_place_in_youtube";

/*var ID_OUR_TAG_SHOW_RANKS_IN_GOOGLE_TRENDS =
    "id_our_tag_show_ranks_in_google_trends";*/

var ID_OUR_TAG_SHOW_RANKS_IN_YANDEX_KEYWORD =
    "id_our_tag_show_ranks_in_yandex_keyword";


var ID_OUR_TAGS_INFORM_BLOCK = 'id_our_tags_inform_block';

var ID_OUR_YANDEX_CAPTCHA_IMG = "id_our_yandex_captcha_img";
var ID_OUR_YANDEX_CAPTCHA_INPUT = "id_our_yandex_captcha_input";
var ID_OUR_YANDEX_CAPTCHA_SUBMIT = "id_our_yandex_captcha_submit";
var ID_OUR_YANDEX_CAPTCHA_RELOAD = "id_our_yandex_captcha_reload";



var ID_OUR_SEARCH_TAGS_PANEL =
    "id_our_search_tags_panel";

var ID_OUR_TAG_UPLOAD_INPUT =
    "fua-plugin-youtube-upload-tags-input";

var ID_OUR_TAG_UPLOAD_ADD_BUTTON =
    "fua-plugin-youtube-upload-tags-add-button";











var ID_OUR_NOT_ADDED_TAGS_BOX =
    "fua-plugin-youtube-no-added-tags-box";

var ID_OUR_SEARCH_PROGRESS =
    "fua-plugin-youtube-search_progress";

var ID_OUR_SEARCH_PROGRESS_PERCENTAGE =
    "fua-plugin-youtube-search_progress_percentage";

var ID_OUR_SEARCH_PROGRESS_LINE =
    "fua-plugin-youtube-search_progress_line";

var ID_OUR_SEARCH_PROGRESS_LINE_INNER =
    "fua-plugin-youtube-search_progress_line_inner";

var ID_OUR_SEARCH_HINTS_BOX=
    "fua-plugin-youtube-search_hints_box";



var ID_OUR_SEARCH_PLACE_PROGRESS =
 "fua-plugin-youtube-search-place_progress";

var ID_OUR_SEARCH_PLACE_PROGRESS_PERCENTAGE =
 "fua-plugin-youtube-search-place_progress_percentage";

var ID_OUR_SEARCH_PLACE_PROGRESS_LINE =
 "fua-plugin-youtube-search-place_progress_line";

var ID_OUR_SEARCH_PLACE_PROGRESS_LINE_INNER =
 "fua-plugin-youtube-search-place_progress_line_inner";



/*var ID_OUR_SEARCH_HINTS_CHECKBOX =
    "fua-plugin-youtube-search_hints_checkbox";*/

var ID_OUR_ADD_HINTS_TO_TAGS_CHECKBOX =
    "fua-plugin-youtube-add_hints_to_tags_checkbox";

var ID_OUR_ADD_TRENDS_TO_TAGS_CHECKBOX =
    "fua-plugin-youtube-add_trends_to_tags_checkbox";

var ID_OUR_ADD_YOUTUBE_TAGS_TO_TAGS_CHECKBOX =
    "fua-plugin-youtube-add_youtube_tags_to_tags_checkbox";

var ID_OUR_ADD_YANDEX_KEYWORDS_TO_TAGS_CHECKBOX =
    "fua-plugin-youtube-add_yandex_keywords_to_tags_checkbox";




var ID_OUR_SHOW_VIDEO_TAGS_BUTTON =
    "fua-plugin-youtube_show_video_tags_button";

var ID_OUR_VIDEO_TAGS_PANEL = "fua-plugin-youtube_video_tags_panel";

var ID_OUR_VIDEO_TAGS_BOX = "fua-plugin-youtube_video_tags_box";

var ID_OUR_VIDEO_TAGS_PANEL_CLOSE =
    "fua-plugin-youtube_video_tags_panel_close";

var ID_OUR_VIDEO_COPY_TAGS_BUTTON =
    "fua-plugin-youtube_video_copy_tags_button";

var ID_OUR_VIDEO_YOUTUBE_PLACE_CIRCLE_LOAD =
    "fua-plugin-youtube_video_youtube_place_circle_load";

var ID_OUR_YANDEX_RANKS_CIRCLE_LOAD =
    "fua-plugin-youtube_yandex_ranks_circle_load";




var ID_OUR_ANNOTATION_COPY_BUTTON =
    "fua-plugin-youtube_annotation_copy_button";

var ID_OUR_ANNOTATION_COPY_BOX = "fua-plugin-youtube_annotation_copy_box";

var ID_OUR_ANNOTATION_VIDEO_SEARCH_INPUT =
    "fua-plugin-youtube_annotation_video_search_input";

var ID_OUR_ANNOTATION_VIDEO_SEARCH_RESULT_BOX =
    "fua-plugin-youtube_annotation_video_search_result_box";






var CLASS_OUR_SEARCH_HINT =
    "fua-plugin-youtube-search_hint";

var CLASS_OUR_NO_CHECKBOX_BUTTON =
    "fua-plugin-youtube-no_checkbox_button";

var CLASS_OUR_TAG_YANDEX_RANK_LABEL =
    "fua-plugin-youtube-tag-yandex-rank-label";

var CLASS_OUR_HIDE_SOMETHING =
    "fua-plugin-youtube-hide-something";

var CLASS_OUR_ANNOTATION_ITEM =
    "fua-plugin-youtube-annotation-item";

var CLASS_OUR_ANNOTATION_ITEM_TITLE =
    "fua-plugin-youtube-annotation-item-title";

var CLASS_OUR_ANNOTATION_ITEM_ANNOTATIONS =
    "fua-plugin-youtube-annotation-item-annotations";

var CLASS_OUR_ANNOTATION_ITEM_ANNOTATION_CHECKBOX =
    "fua-plugin-youtube-annotation-item-annotations-checkbox";

var CLASS_OUR_ADD_ANNOTATIONS_TO_START =
    "fua-plugin-youtube_annotations_to_start";

var CLASS_OUR_ADD_ANNOTATIONS_TO_END =
    "fua-plugin-youtube_annotations_to_end";



//get youtube tags
var  QUANTITY_YOUTUBE_SEARCH_PAGES = 1;
var  MAX_TAGS_TOTAL_LENGTH = 500;
var  MAX_TITLE_LENGTH = 100;
var  MAX_DESCRIPTION_LENGTH = 5000;


var TIME_RANGE_POSITION = [ [ 0, 1 ],  [ 2, 4 ],  [ 5, 7 ],  [ 8, 9 ] ];
var MILLISECOND_IN = {
    hour : 3600000,
    minute : 60000,
    second : 1000
}


// Original youtube selectors
var SELECTORS_YOUTUBE_TAGS_BOX =
    "div.video-settings-tag-chips-container:first";

var SELECTORS_YOUTUBE_ADD_TAG_INPUT =  "input.video-settings-add-tag";


var SELECTORS_YOUTUBE_WATCH_VIDEO_TAGS =
    'span.fua-plugin-youtube-watch-video-tags';


// Our selectors
var SELECTORS_OUR_YOUTUBE_PLACE_CIRCLE_LOAD =
    '#' + ID_OUR_VIDEO_YOUTUBE_PLACE_CIRCLE_LOAD +
    ' .fua-circle-load-progress-pie-chart';

var SELECTORS_OUR_YANDEX_KEYWORDS_CIRCLE_LOAD =
    '#' + ID_OUR_YANDEX_RANKS_CIRCLE_LOAD +
    ' .fua-circle-load-progress-pie-chart';


// Context menu
var CONTEXT_MENU_ADD_VIDEO_TIME_CODE =
    'CONTEXT_MENU_ADD_VIDEO_TIME_CODE';



// Random string
var RANDOM_STRING_NUMBERS_ONLY = "0123456789";
var RANDOM_STRING_LETTERS_ONLY =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
var RANDOM_STRING_LETTERS_AND_NUMBERS =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";