var FUA_YT_YANDEX_KW = (function(){

    function Yandex_keywords(){
        this.prefix = 'fua_youtube_';
        this.id = {
            'horizontal_block' : this.prefix + 'yandex_kw_horizontal_block_id',
            'control_panel' : this.prefix + 'yandex_kw_control_panel_id',
            'h_progress_bar' : this.prefix + 'yandex_kw_h_progress_bar_id',
        };
        this.class = {
        };
    }


    return new Yandex_keywords();
})();